from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO

def update_account(ch, method, properties, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)
    if is_active:
        AccountVO.objects.update_or_create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            is_active=is_active,
            updated=updated,
        )
    else:
        AccountVO.objects.filter(email=email).delete()

while True:
    try:
        # Creating the pika Connection parameters

        parameters = pika.ConnectionParameters(host='rabbitmq')

        # Blocking content
        connection = pika.BlockingConnection(parameters)

        # Open Channel
        channel = connection.channel()

        #declare a fanout exchange named account_info
        channel.exchange_declare(exchange="account_info", exchange_type="fanout")

        #declare a randomly - named queue
        result = channel.queue_declare(queue=' ', exclusive=True)

        #get the queue name of the randomly-named queue
        queue_name = result.method.queue

        # Bind the queue to account info exchange
        channel.queue_bind(exchange="account_info", queue=queue_name)

        # do a basic_consume for the queue name that calls for the function
        channel.basic_consume(queue=queue_name, on_message_callback=update_account, auto_ack=True)

        #start consuming
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ. retrying in 2 seconds....")
        time.sleep(2)
